import React, { useState, useEffect } from 'react'
import web3 from "./web3";
import lottery from "./lottery";
import './App.css';

function App() {
    const [manager, setManager] = useState(null)
    const [isConnected, setIsConnected] = useState(false)
    const [players, setPlayers] = useState([])
    const [balance, setBalance] = useState('')
    const [value, setValue] = useState('')
    const [isManager, setIsManager] = useState(false)
    const [entering, setEntering] = useState('To participate, please enter to the lottery!')

    useEffect(() => {
        fetchData()
    }, [])
    async function fetchData() {
        const manager = await lottery.methods.manager().call();
        setManager(manager)
        const accounts = await web3.eth.getAccounts()
        
        if (accounts[0] === manager) {
            setIsManager(true)
        }
        if (accounts.length > 0) {
            setIsConnected(true)
        }
        const players = await lottery.methods.getPlayers().call();
        setPlayers(players)
        const balance = await web3.eth.getBalance(lottery.options.address)
        setBalance(balance)
    }

    const onSubmit = async  (event) => {
        //Form submit handler
        event.preventDefault()
        setEntering('Entering you to the lottery!')
        const accounts = await web3.eth.getAccounts()

        try {
            let txn = await lottery.methods.enter().send({
                from: accounts[0],
                value: web3.utils.toWei(value, 'ether')
            })
            fetchData();
            setEntering("You have been entered to the lottery!\n\n Tx: " + txn.transactionHash)
        } catch (e) {
            alert(e.message)
        }
    }

    const pickWinner = async (event) => {
        setEntering("Picking Winner!")
        const accounts = await web3.eth.getAccounts()
        try {
            let txn = await lottery.methods.pickWinner().send({
                from: accounts[0]
            })
            setEntering("A winner has been picked! Tx: " + txn.transactionHash)
            fetchData()
        } catch (e) {
            alert(e.message)
        }
    }

  return (
    <div className="App">
        <h1>Lottery Contract</h1>
        <p>
            <strong>Lottery Manager: </strong>
            {manager}
        </p>
        { !isConnected && <div className="how-to-div">
            <h3>You are not connected.</h3>
            <ol>
                <li>Go to <strong>MetaMask</strong> <span className="arrow">↦</span> <strong>Account Option</strong> <span className="arrow">↦</span> <strong>Connected Sites</strong></li>
                <li>Click on <strong>Manually Connect to current site</strong> </li>
            </ol>
        </div>}
        <hr/>
        <p>There are currently {players.length} people entered, competing to win {web3.utils.fromWei(balance, 'ether')} ether!</p>

        <hr/>
        <form onSubmit={onSubmit}>
            <h4>Want to try your luck?</h4>
            <label>Amount of ether to enter</label>
            <input
                onChange={event => setValue(event.target.value)}
                value={value}
            />
            <br/>
            <button>Enter!</button>
        </form>
        <br/>
        <p>{entering}</p>
        { isManager && (<div>
            <br/><hr/><br/>
            <h4>Ready to pick a winner?</h4>
            <button onClick={pickWinner}>Pick Winner!</button>
        </div>)}



    </div>
  );
}

export default App;
